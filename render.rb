#!/usr/bin/env ruby

#
# gem install liquid json
#

require 'liquid'
require 'json'

template_file = File.read("template.html")
data_file = File.read("data.json")

begin
    data = JSON.parse(data_file)
    p "Parse Succeeded"
rescue JSON::ParserError => e
    p "Parse Failed: #{e.message[0..200]}..."
    exit(1)
end

@template = Liquid::Template.parse(template_file)
output = @template.render(data)

File.write("index.html", output);
